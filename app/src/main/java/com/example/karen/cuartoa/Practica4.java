package com.example.karen.cuartoa;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Practica4 extends AppCompatActivity implements View.OnClickListener, FrgUn.OnFragmentInteractionListener, FrgDos.OnFragmentInteractionListener {

    Button botonFragUno, botonFragDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practica4);
        botonFragUno = (Button) findViewById(R.id.btnFrgUno);
        botonFragDos = (Button) findViewById(R.id.btnFrgDos);
        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUno:
            FrgUn fragmentoUno = new FrgUn();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrgDos:
                FrgDos fragmentoDos = new FrgDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragmentoDos);
                transactionDos.commit();
                break;
         }


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(Practica4.this);
                dialogoLogin.setContentView(R.layout.dlg_login);

                Button botonAutenticar = (Button) dialogoLogin.findViewById(R.id.btnAutentificar);
                final EditText cajaUsuario = (EditText) dialogoLogin.findViewById(R.id.txtUsuario);
                final EditText cajaClave = (EditText) dialogoLogin.findViewById(R.id.txtContrasena);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Practica4.this, cajaUsuario.getText().toString() + "" + cajaClave.getText().toString(), Toast.LENGTH_LONG).show();

                    }
                });

                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
                break;
        }
        return true;
    }
}
